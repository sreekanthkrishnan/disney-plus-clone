import React from "react";
import styled from "styled-components";
import pic from "../../assets/images/bgimage.jpg";

function Login() {
    return (
        <MainContainer>
            <Content>
                <TopLogoBanner>
                    <Image src={require("../../assets/images/login-banner-top-img.svg").default} />
                </TopLogoBanner>
                <ContinueButton>Get all there</ContinueButton>
                <Description>
                    Disney classics, Pixar adventures, Marvel epics, Star Wars sagas, National
                    Geographic explorations, and more. The new home for your favorites. An
                    unprecedented collection of the world's most beloved movies and TV series. See
                    More. Get The Disney Bundle. Stream now or download and go.
                </Description>
                <BottomLogoBanner>
                    <Image
                        src={require("../../assets/images/login-banner-bottom-img.png").default}
                    />
                </BottomLogoBanner>
            </Content>
        </MainContainer>
    );
}

export default Login;

const MainContainer = styled.section`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-image: url(${pic});
    background-size: cover;
    position: relative;
`;
const Content = styled.div`
    width: 40%;
    max-width: 650px;
    @media all and (max-width: 768px) {
        width: 70%;
    }
    @media all and (max-width: 480px) {
        width: 300px;
    }
    @media all and (max-width: 360px) {
        width: 250px;
    }
`;
const TopLogoBanner = styled.div`
    width: 100%;
    margin-bottom: 12px;
`;
const Image = styled.img`
    width: 100%;
`;
const ContinueButton = styled.span`
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 22px;
    font-weight: 700;
    letter-spacing: 0.2rem;
    text-transform: uppercase;
    width: 100%;
    height: 70px;
    color: #f9f9f9;
    background-color: #1156f5;
    margin-bottom: 12px;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    &:hover {
        background-color: #0483ee;
        letter-spacing: 0.12rem;
    }
    @media all and (max-width: 768px) {
        height: 60px;
    }
    @media all and (max-width: 480px) {
        height: 50px;
        font-size: 18px;
    }
    @media all and (max-width: 360px) {
        height: 40px;
    }
`;
const Description = styled.p`
    text-align: center;
    margin-bottom: 12px;
    @media all and (max-width: 480px) {
        font-size: 15px;
    }
    @media all and (max-width: 360px) {
        font-size: 14px;
    }
`;
const BottomLogoBanner = styled.div`
    width: 100%;
`;
