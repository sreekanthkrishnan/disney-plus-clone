import React from "react";
import styled from "styled-components";

function Navbar() {
    return (
        <Container>
            <LeftSection>
                <Logo>
                    <Image src={require("../../assets/images/logo.svg").default} alt="logo" />
                </Logo>
            </LeftSection>
            <RightSection>
                <MenuSection></MenuSection>
                <LoginButtom>Log in</LoginButtom>
            </RightSection>
        </Container>
    );
}

export default Navbar;

const Container = styled.div`
    width: 100%;
    background-color: #141b28;
    padding: 10px 30px;
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 99999;
`;
const Logo = styled.h1`
    width: 100px;
`;

const Image = styled.img`
    width: 100%;
`;
const LoginButtom = styled.span`
    text-transform: uppercase;
    font-size: 18px;
    letter-spacing: 0.12rem;
    width: 100px;
    border: 1px solid #fff;
    height: 42px;
    border-radius: 4px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    &:hover {
        opacity: 0.9;
    }
`;
const LeftSection = styled.div``;
const RightSection = styled.div``;
const MenuSection = styled.div``;
