const firebaseConfig = {
    apiKey: "AIzaSyDxuuY7-0zOMnkUM92gSZEFK2qedG1153o",
    authDomain: "disney-plus-clone-23147.firebaseapp.com",
    projectId: "disney-plus-clone-23147",
    storageBucket: "disney-plus-clone-23147.appspot.com",
    messagingSenderId: "631434634850",
    appId: "1:631434634850:web:445f12016d25ade1f2933b",
    measurementId: "G-LLKRM7W5L0",
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
const storage = firebase.storage();

export { auth, provider, storage };
export default db;
