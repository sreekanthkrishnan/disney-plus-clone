import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import styled from "styled-components";
import Login from "./components/screens/Login";
import Navbar from "./components/includes/Navbar";

function App() {
    return (
        <Container className="App">
            <Navbar />
            <Router>
                <Switch>
                    <Route exact path="/" component={Login} />
                </Switch>
            </Router>
        </Container>
    );
}

export default App;

const Container = styled.div`
    position: relative;
`;
